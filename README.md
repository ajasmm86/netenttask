# README #

### What is this repository for? ###

* The assignment uploaded is  a small, full stack javascript application. The application is a simple game
  which can display a random outcome generated on the server.The outcomes are shown as images randomly served from server.

### How do I get set up? ###

* The repo has two distinct folders 
* NetEnt-API - The Folder holds the backend code written in node.js.The readme.md file in the  path netenttask\NetEnt-API  holds     information on getting started with the server application.

* NetEnt-Frontend - - The Folder holds the frontend code written in javascript  compliant with ES6 standard.The readme.md file in  the  path netenttask\NetEnt-Frontend   holds information on getting started with the frontend application.


### Who do I talk to? ###

* ajasmm86@gmail.com
