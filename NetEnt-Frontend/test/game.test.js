/**
 * Test module
*/

import GameController from "../js/controllers/game.controller"; 
let chai = require('chai');
let expect = chai.expect;

describe("Game Controller", function() {
  describe("constructor calling valid object is created", function() {
    it("should have a default name", function() {
      var gameCtrl = new GameController();
      expect(gameCtrl).to.be.an('object');
    });
  });

describe("#promise", function() {
    it("should throw if no promise  is passed in", function() {
      expect(function() {
        const resolvingPromise = (new GameController()).onFetchScores();
        resolvingPromise.then( (result) => {
            expect(result).to.equal('promise resolved');
          });
      })
    });

  });
});