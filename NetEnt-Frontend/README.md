
#  NETENT Game Application frontend implementation

## Requirements

For development, you will  need Node.js environment installed on your system.

## Install

    $ git clone https://ajasmm86@bitbucket.org/ajasmm86/netenttask.git
    $ cd to netenttask\NetEnt-Frontend
    $ npm install

### Configure app

    Change Api Environment, if needed inside utils/constants.js

## Start

    $ npm start - This command will start the application in localhost.

## Simple build for development

    $ npm run webpack - This command will help affect changes once code is modified and take a build if needed.
    
## Unit Test 

    $ npm test - This command will recursively run test cases defined

## Languages & tools

 * HTML
 * JavaScript(ES6)
 * CSS
 * webpack -javascript module bundler


## Tested Environments

- Google chrome
- Mozilla Firefox
- iPad Pro
- Galaxy S III