/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/*******************************************************************
   class exports global Constants 
********************************************************************/
var Constants = exports.Constants = {
  cEnv: {
    _API_ENV: "http://127.0.0.1:3000/"
  },
  cApiEndPoints: {
    _SCORES_API: "api/v1/fetchResults"
  },
  cStrConstants: {
    IMAGE_ID: "image",
    ANIM_FRAME_ID1: "frame1",
    ANIM_FRAME_ID2: "frame2",
    STARTBTN: "lBtn",
    CLICK: "click",
    ANIM_FRAME_CLASS1: "frame-1",
    ANIM_FRAME_CLASS2: "frame-2",
    WBKIT_ANIM_END: "webkitAnimationEnd"
  }

};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _game = __webpack_require__(2);

var _game2 = _interopRequireDefault(_game);

var _constants = __webpack_require__(0);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*Game initialization logic */
/*******************************************************************
   The Game Application Entry point 
********************************************************************/
document.getElementById(_constants.Constants.cStrConstants.STARTBTN).addEventListener(_constants.Constants.cStrConstants.CLICK, function () {
    var gameObj = new _game2.default();
    gameObj.onFetchScores();
});

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /*******************************************************************
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        The view-controller class which handles the game logic 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ********************************************************************/


var _constants = __webpack_require__(0);

var _restUtility = __webpack_require__(3);

var _restUtility2 = _interopRequireDefault(_restUtility);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* 
 * Setting up block level variable to store class state
 * , set's to null by default.
 */
var instance = null;

var GameController = function () {
  function GameController() {
    _classCallCheck(this, GameController);

    if (!instance) {
      this._data = [];
      instance = this;
      this._bonusFactor = {};
      this.onBonusActivation = this.onBonusActivation.bind(this);
    }
    return instance;
  }

  /********************************************************************************
   Name - onFetchScores
   Description - Function fetches game outcomes with image src's and display results 
   Arguments   - Null 
   ReturnValue - Promise call back with resultant json response
  *********************************************************************************/

  _createClass(GameController, [{
    key: 'onFetchScores',
    value: function onFetchScores() {
      var _this = this;

      this._bonusFactor = {};
      _restUtility2.default.handleGetRequests(_constants.Constants.cApiEndPoints._SCORES_API).then(function (data) {
        if (data.response) {
          data.response.imgArray.map(function (imgSrc, index) {
            document.getElementById(_constants.Constants.cStrConstants.IMAGE_ID + index).src = _constants.Constants.cEnv._API_ENV + imgSrc;
          });
          _this.animateFn({
            id: _constants.Constants.cStrConstants.ANIM_FRAME_ID1,
            class: _constants.Constants.cStrConstants.ANIM_FRAME_CLASS1,
            message: data.response.sucesssFactor.spinMessage
          });
          if (data.response.bonusFactor.bonusFlag) {
            _this._bonusFactor = data.response.bonusFactor;
            document.getElementById(_constants.Constants.cStrConstants.ANIM_FRAME_ID2).addEventListener(_constants.Constants.cStrConstants.WBKIT_ANIM_END, _this.onBonusActivation);
            _this.animateFn({
              id: _constants.Constants.cStrConstants.ANIM_FRAME_ID2,
              class: _constants.Constants.cStrConstants.ANIM_FRAME_CLASS2,
              message: data.response.bonusFactor.bonusMessage
            });
            _this._bonusFactor = {};
          }
        }
      }).catch(function (error) {
        console.log(error);
      });
    }

    /********************************************************************************
     Name - animateFn
     Description - Restart's Animation 
     Arguments   - Data object with element id,class and message 
     ReturnValue - nil
    *********************************************************************************/

  }, {
    key: 'animateFn',
    value: function animateFn(data) {
      var animDivElem = document.getElementById(data.id);
      animDivElem.classList.remove(data.class);
      void animDivElem.offsetWidth;
      animDivElem.innerHTML = data.message;
      animDivElem.classList.add(data.class);
    }

    /********************************************************************************
     Name - onBonusActivation
     Description - Restart Game play on recieving bonus 
     Arguments   - nil 
     ReturnValue - nil
    *********************************************************************************/

  }, {
    key: 'onBonusActivation',
    value: function onBonusActivation() {
      document.getElementById(_constants.Constants.cStrConstants.ANIM_FRAME_ID2).removeEventListener(_constants.Constants.cStrConstants.WBKIT_ANIM_END, this.onBonusActivation);
      this.onFetchScores();
    }
  }]);

  return GameController;
}();

exports.default = GameController;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /*******************************************************************
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          Utility Class which routes all rest api calls
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ********************************************************************/


var _constants = __webpack_require__(0);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ServiceUtil = function () {
    function ServiceUtil() {
        _classCallCheck(this, ServiceUtil);
    }

    /*******************************************************************
     Name - handleGetRequests
     Description - Function Handles all Get Requests 
     Arguments   - API Endpoint 
     ReturnValue - Promise call back with resultant json response
    ********************************************************************/

    _createClass(ServiceUtil, null, [{
        key: 'handleGetRequests',
        value: function handleGetRequests(apiEndPoint) {
            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            var apiUrl = _constants.Constants.cEnv._API_ENV + apiEndPoint;
            return fetch(apiUrl).then(function (resp) {
                return resp.json();
            });
        }
    }]);

    return ServiceUtil;
}();

exports.default = ServiceUtil;

/***/ })
/******/ ]);
//# sourceMappingURL=main.bundle.js.map