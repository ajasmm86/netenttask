/*******************************************************************
   The view-controller class which handles the game logic 
********************************************************************/
import {
  Constants
} from '../utils/constants';
import UtilService from '../services/rest-utility.service';

/* 
 * Setting up block level variable to store class state
 * , set's to null by default.
 */
let instance = null;
export default class GameController {
  constructor() {
    if (!instance) {
      this._data = [];
      instance = this;
      this._bonusFactor = {};
      this.onBonusActivation = this.onBonusActivation.bind(this);
    }
    return instance;
  }

  /********************************************************************************
   Name - onFetchScores
   Description - Function fetches game outcomes with image src's and display results 
   Arguments   - Null 
   ReturnValue - Promise call back with resultant json response
  *********************************************************************************/

  onFetchScores() {
    return new Promise((resolve, reject) => {
      this._bonusFactor = {};
      UtilService.handleGetRequests(Constants.cApiEndPoints._SCORES_API).then((data) => {
        if (data.response) {
          data.response.imgArray.map(function (imgSrc, index) {
            document.getElementById(Constants.cStrConstants.IMAGE_ID + index).src = Constants.cEnv._API_ENV + imgSrc;
          })
          this.animateFn({
            id: Constants.cStrConstants.ANIM_FRAME_ID1,
            class: Constants.cStrConstants.ANIM_FRAME_CLASS1,
            message: data.response.sucesssFactor.spinMessage
          });
          if (data.response.bonusFactor.bonusFlag) {
            this._bonusFactor = data.response.bonusFactor;
            document.getElementById(Constants.cStrConstants.ANIM_FRAME_ID2).addEventListener(Constants.cStrConstants.WBKIT_ANIM_END, this.onBonusActivation);
            this.animateFn({
              id: Constants.cStrConstants.ANIM_FRAME_ID2,
              class: Constants.cStrConstants.ANIM_FRAME_CLASS2,
              message: data.response.bonusFactor.bonusMessage
            });
            this._bonusFactor = {};
          }
          resolve('promise resolved');
        } else {
          reject('promise rejected');
        }
      }).catch(function (error) {
        console.log(error);
        reject('promise rejected');
      })
    });
  }

  /********************************************************************************
   Name - animateFn
   Description - Restart's Animation 
   Arguments   - Data object with element id,class and message 
   ReturnValue - nil
  *********************************************************************************/
  animateFn(data) {
    let animDivElem = document.getElementById(data.id);
    animDivElem.classList.remove(data.class);
    void animDivElem.offsetWidth;
    animDivElem.innerHTML = data.message;
    animDivElem.classList.add(data.class);
  }


  /********************************************************************************
   Name - onBonusActivation
   Description - Restart Game play on recieving bonus 
   Arguments   - nil 
   ReturnValue - nil
  *********************************************************************************/
  onBonusActivation() {
    document.getElementById(Constants.cStrConstants.ANIM_FRAME_ID2).removeEventListener(Constants.cStrConstants.WBKIT_ANIM_END, this.onBonusActivation);
    this.onFetchScores();
  }
}