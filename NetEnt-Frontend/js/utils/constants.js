/*******************************************************************
   class exports global Constants 
********************************************************************/
export const Constants = {
  cEnv: {
    _API_ENV: "http://127.0.0.1:3000/"
  },
  cApiEndPoints: {
    _SCORES_API: "api/v1/fetchResults"
  },
  cStrConstants: {
    IMAGE_ID: "image",
    ANIM_FRAME_ID1: "frame1",
    ANIM_FRAME_ID2: "frame2",
    STARTBTN: "lBtn",
    CLICK: "click",
    ANIM_FRAME_CLASS1: "frame-1",
    ANIM_FRAME_CLASS2: "frame-2",
    WBKIT_ANIM_END: "webkitAnimationEnd"
  }

}