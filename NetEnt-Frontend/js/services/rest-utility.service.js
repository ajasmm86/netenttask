/*******************************************************************
     Utility Class which routes all rest api calls
********************************************************************/
import {Constants} from '../utils/constants';

export default class ServiceUtil {
    constructor() {
    }
    
    /*******************************************************************
     Name - handleGetRequests
     Description - Function Handles all Get Requests 
     Arguments   - API Endpoint 
     ReturnValue - Promise call back with resultant json response
    ********************************************************************/
    
    static handleGetRequests(apiEndPoint) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let apiUrl = Constants.cEnv._API_ENV + apiEndPoint;
        return fetch(apiUrl).then((resp) => resp.json());
    }
}