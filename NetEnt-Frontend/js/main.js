/*******************************************************************
   The Game Application Entry point 
********************************************************************/
import GameController from "./controllers/game.controller";
import {Constants} from './utils/constants';

/*Game initialization logic */
document.getElementById(Constants.cStrConstants.STARTBTN).addEventListener(Constants.cStrConstants.CLICK, function () {
    let gameObj = new GameController();
    gameObj.onFetchScores();
});