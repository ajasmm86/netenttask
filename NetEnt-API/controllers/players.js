/***********************************************************************************
    class handling controller logistics 
    
***********************************************************************************/
'use strict';
let PlayersService = require('../services/players.service');

class PlayersController {
    constructor(router) {
        this.router = router;
        this.registerRoutes();
    }

    registerRoutes() {
        this.router.get('/', this.fetchDrawResult.bind(this));
    }


    /*********************************************************
     Name - fetchDrawResult
     Description - Function to fetch result from data layer 
     Arguments   - req,res handlers 
     ReturnValue - json response resolution to response handler
    ***********************************************************/

    fetchDrawResult(req, res) {
        let resultObj = PlayersService.getResultObject();
        res.send(resultObj);
    }


}

module.exports = PlayersController;