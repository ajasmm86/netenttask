 
# NETENT Game Application Backend implementation

## Requirements

For development, you will  need Node.js environment installed on your system.The code was developed using visual studio code IDE.

## Install

    $ git clone https://ajasmm86@bitbucket.org/ajasmm86/netenttask.git
    $ cd to netenttask\NetEnt-API directory
    $ npm install

## Start

    $ npm start - This command will start the application in localhost with default port 3000.

## Simple build for development

    $ npm run build - This command will help affect changes once code is modified and take a build.

## Unit Test 

    $ npm test - This command will recursively run test cases defined


## Languages & tools

  * Node.js-ES6 compliant
  * Babel-used for Transpiling ES6 code to javascript
  * Mocha and chai for unit test case running and assertion.

## Tested Environments

- Google chrome
- Mozilla Firefox
- iPad Pro
- Galaxy S III