/*******************************************************************
    Application entry point
********************************************************************/
'use strict';
let express = require('express'),
    app = express();
let bodyParser = require('body-parser');
const path = require('path');
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public/images')));

let cors = require('cors');
app.use(cors());

let apiRouter = express.Router();
app.use('/api', apiRouter);

let apiV1 = express.Router();
apiRouter.use('/v1', apiV1);

let playersApiV1 = express.Router();
apiV1.use('/fetchResults', playersApiV1);


let PlayersController = require('./controllers/players');
let pc = new PlayersController(playersApiV1);

// start the server
let server = app.listen(3000, () => {
    let host = server.address().address;
    host = (host === '::' ? 'localhost' : host);
    let port = server.address().port;

    console.log('listening at http://%s:%s', host, port);
});

module.exports = app;