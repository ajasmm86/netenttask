/******************************************************************************
The test implementation logic 
******************************************************************************/
process.env.NODE_ENV = 'test';
let PlayersService = require('../services/players.service');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
const expect = require('chai').expect;

chai.use(chaiHttp);

describe('/GET scores', () => {
  it('it should GET random draw results', (done) => {
    chai.request(server)
      .get('/api/v1/fetchResults')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body.response.imgArray).to.be.an('array');
        expect(res.body.response.imgArray).to.have.lengthOf(3);
        done();
      });
  });
});


// GET - Invalid path
it('should return Not Found', function () {
  return chai.request(server)
    .get('/INVALID_PATH')
    .then(function (res) {
      throw new Error('Path exists!');
    })
    .catch(function (err) {
      expect(err).to.have.status(404);
    });
});