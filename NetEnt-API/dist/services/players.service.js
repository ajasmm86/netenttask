/******************************************************************************
    
The service layer class which handles the data fetch/db interactions if any 

******************************************************************************/
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var uuid = require('node-uuid');
var Constants = require('../utils/constants');
var fs = require('fs');

var PlayersService = function () {
    function PlayersService() {
        _classCallCheck(this, PlayersService);

        this.players = [];
        this.resultObj = {
            response: {
                imgArray: null,
                sucesssFactor: null,
                status: null,
                bonusFactor: null
            }

        };
    }

    /*******************************************************************
     Name - getResultObject
     Description - JSON response generator with Win factors and bonus triggered details 
     Arguments   - nil
     ReturnValue - JSON object with images src array,win details,bonus triggered details
     ********************************************************************/


    _createClass(PlayersService, [{
        key: 'getResultObject',
        value: function getResultObject() {
            var randNumArr = [];
            var randImgArr = [];
            for (var i = 0; i < Constants.cStrConstants.KRESULTCOUNT; i++) {
                randNumArr.push(this.getRandomInt(Constants.cStrConstants.KMIN, Constants.cStrConstants.KMAX));
            }

            //Commenting out simple approach to fetch images more generic approach has been used below 
            /*
            randNumArr.forEach(function (value) {
                randImgArr.push(Constants.cStrConstants.KSYMBOL + value + Constants.cStrConstants.KTYPE);
            });
            */

            fs.readdirSync(Constants.cStrConstants.KIMAGEFOLDER).forEach(function (file) {
                var fileNumber = parseInt(file.substring(file.lastIndexOf("_") + 1, file.lastIndexOf(".")));
                for (var _i = 0; _i < randNumArr.length; _i++) {
                    if (randNumArr[_i] == fileNumber) {
                        randImgArr.push(file);
                    }
                }
            });

            var resultObjClone = JSON.parse(JSON.stringify(this.resultObj));
            resultObjClone.response.imgArray = randImgArr;
            resultObjClone.response.sucesssFactor = this.getSuccessFactor(randNumArr);
            resultObjClone.response.bonusFactor = this.getBonusFactor(resultObjClone.response.sucesssFactor.winFactor, Constants.cStrConstants.KMAX);
            resultObjClone.response.status = Constants.cStrConstants.KSUCCESS;
            return resultObjClone;
        }

        /*******************************************************************
         Name - getRandomInt
         Description - Random Number generator function 
         Arguments   - min,max boundaries
         ReturnValue - Random number between min and max
        ********************************************************************/

    }, {
        key: 'getRandomInt',
        value: function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }
    }, {
        key: 'getSuccessFactor',


        /*******************************************************************
         Name - getSuccessFactor
         Description - Function that fetches the Success factor(NO_WIN,SMALL_WIN,BIG_WIN) from random array 
         Arguments - Array of randomly generated  Numbers
         ReturnValue - JSON obejct with bonus successFactor details and win message
        ********************************************************************/
        value: function getSuccessFactor(randNumArr) {
            var winFactor = null;
            var spinMessage = null;

            var uniqueVal = randNumArr.filter(function (v, i, a) {
                return a.indexOf(v) === i;
            });
            switch (uniqueVal.length) {
                case 0:
                    {
                        winFactor = Constants.cStrConstants.KNO_WIN;
                        spinMessage = Constants.cStrConstants.KNWIN_MSG;
                        break;
                    }
                case 1:
                    {
                        winFactor = Constants.cStrConstants.KBIG_WIN;
                        spinMessage = Constants.cStrConstants.KBWIN_MSG;
                        break;
                    }
                case 2:
                    {
                        winFactor = Constants.cStrConstants.KSMALL_WIN;
                        spinMessage = Constants.cStrConstants.KSWIN_MSG;
                        break;
                    }
                default:
                    {
                        winFactor = Constants.cStrConstants.KNO_WIN;
                        spinMessage = Constants.cStrConstants.KNWIN_MSG;
                        break;
                    }
            }
            return {
                "winFactor": winFactor,
                "spinMessage": spinMessage
            };
        }
    }, {
        key: 'getBonusFactor',


        /*******************************************************************
        Name - getBonusFactor
        Description - Function that checks for bonus applicable for  NO_WIN and SMALL_WIN scenarios
        Arguments - Success Factor (NO_WIN,SMALL_WIN,BIG_WIN)
        ReturnValue - JSON obejct with bonus triggered details and win message
        ********************************************************************/
        value: function getBonusFactor(successFactor, max) {
            var bonusFlag = false;
            var bonusMessage = null;
            if (successFactor != Constants.cStrConstants.BIG_WIN) {
                if (Math.floor(Math.random() * (max - 0 + 1) + 0) == max - 1) {
                    bonusFlag = true;
                    bonusMessage = Constants.cStrConstants.KBONUS_WIN_MSG;
                }
            }
            return {
                "bonusFlag": bonusFlag,
                "bonusMessage": bonusMessage
            };
        }
    }]);

    return PlayersService;
}();

module.exports = new PlayersService();