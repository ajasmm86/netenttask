"use strict";

/*******************************************************************
   class exports global Constants 
********************************************************************/
var Constants = {
    cStrConstants: {
        KNO_WIN: "No Win",
        KSMALL_WIN: "Small Win",
        KBIG_WIN: "Big Win",
        KSUCCESS: "success",
        KBWIN_MSG: "Awsome !  Big Win",
        KSWIN_MSG: "WOW!  Small Win",
        KNWIN_MSG: "Hard Luck! No Win",
        KBONUS_WIN_MSG: "WOW! You have Won a Bonus",
        KSYMBOL: "Symbol_",
        KTYPE: ".PNG",
        KIMAGEFOLDER: './public/images',
        KMIN: 0,
        KMAX: 5,
        KRESULTCOUNT: 3
    }
};

module.exports = Constants;