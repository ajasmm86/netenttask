/***********************************************************************************
    class handling controller logistics 
    
***********************************************************************************/
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PlayersService = require('../services/players.service');

var PlayersController = function () {
    function PlayersController(router) {
        _classCallCheck(this, PlayersController);

        this.router = router;
        this.registerRoutes();
    }

    _createClass(PlayersController, [{
        key: 'registerRoutes',
        value: function registerRoutes() {
            this.router.get('/', this.fetchDrawResult.bind(this));
        }

        /*********************************************************
         Name - fetchDrawResult
         Description - Function to fetch result from data layer 
         Arguments   - req,res handlers 
         ReturnValue - json response resolution to response handler
        ***********************************************************/

    }, {
        key: 'fetchDrawResult',
        value: function fetchDrawResult(req, res) {
            var resultObj = PlayersService.getResultObject();
            res.send(resultObj);
        }
    }]);

    return PlayersController;
}();

module.exports = PlayersController;