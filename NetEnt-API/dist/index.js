/*******************************************************************
    Application entry point
********************************************************************/
'use strict';

var express = require('express'),
    app = express();
var bodyParser = require('body-parser');
var path = require('path');
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public/images')));

var cors = require('cors');
app.use(cors());

var apiRouter = express.Router();
app.use('/api', apiRouter);

var apiV1 = express.Router();
apiRouter.use('/v1', apiV1);

var playersApiV1 = express.Router();
apiV1.use('/fetchResults', playersApiV1);

var PlayersController = require('./controllers/players');
var pc = new PlayersController(playersApiV1);

// start the server
var server = app.listen(3000, function () {
    var host = server.address().address;
    host = host === '::' ? 'localhost' : host;
    var port = server.address().port;

    console.log('listening at http://%s:%s', host, port);
});

module.exports = app;