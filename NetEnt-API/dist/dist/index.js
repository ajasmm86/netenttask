
/*******************************************************************
    Application entry point
********************************************************************/
'use strict';

var express = require('express'),
    app = express();
var bodyParser = require('body-parser');
var path = require('path');
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public/images')));

var cors = require('cors');
app.use(cors());

var apiRouter = express.Router();
app.use('/api', apiRouter);

var apiV1 = express.Router();
apiRouter.use('/v1', apiV1);

var playersApiV1 = express.Router();
apiV1.use('/players', playersApiV1);

var PlayersController = require('./controllers/players');
var pc = new PlayersController(playersApiV1);

// seed the data layer
var PlayersService = require('./services/players');

// Swagger Docs
var swaggerTools = require('swagger-tools');
// swaggerRouter configuration
var options = {
    swaggerUi: '/swagger.json',
    controllers: './controllers'
};

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var swaggerDoc = require('./swagger.json');

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {
    // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
    app.use(middleware.swaggerMetadata());

    // Validate Swagger requests
    app.use(middleware.swaggerValidator());

    // Route validated requests to appropriate controller
    app.use(middleware.swaggerRouter(options));

    // Serve the Swagger documents and Swagger UI
    app.use(middleware.swaggerUi());

    // start the server
    var server = app.listen(3000, function () {
        var host = server.address().address;
        host = host === '::' ? 'localhost' : host;
        var port = server.address().port;

        console.log('listening at http://%s:%s', host, port);
    });
});

module.exports = app;