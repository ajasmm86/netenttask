'use strict';

/******************************************************************************
The test implementation logic 
******************************************************************************/

process.env.NODE_ENV = 'test';
var PlayersService = require('../services/players');

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../index');
var should = chai.should();
var expect = require('chai').expect;

chai.use(chaiHttp);

describe('/GET scores', function () {
  it('it should GET random draw results', function (done) {
    chai.request(server).get('/api/v1/players').end(function (err, res) {
      expect(res).to.have.status(200);
      expect(res).to.be.json;
      expect(res.body).to.be.an('object');
      expect(res.body.response.imgArray).to.be.an('array');
      expect(res.body.response.imgArray).to.have.lengthOf(3);
      done();
    });
  });
});

// GET - Invalid path
it('should return Not Found', function () {
  return chai.request(server).get('/INVALID_PATH').then(function (res) {
    throw new Error('Path exists!');
  }).catch(function (err) {
    expect(err).to.have.status(404);
  });
});